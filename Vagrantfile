# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'
config_file=File.expand_path(File.join(File.dirname(__FILE__), 'vagrant_variables.yml'))
settings=YAML.load_file(config_file)

### BEGIN Vars - SET VALUES IN settings.yml
BOX = settings['box']
MASTER_RAM = settings['master_ram']
MASTER_CPUS = settings['master_cpus']
NODE_COUNT = settings['node_count']
NODE_RAM = settings['node_ram']
NODE_CPUS = settings['node_cpus']
SUBNET = settings['subnet']
INIT_ARGS = settings['init_args']
LINKED_CLONES = settings['linked_clones']
PACKET_CAPTURE = settings['packet_capture']
CEPH = settings['ceph']
BOX_HAS_SCSI = settings['box_has_scsi']
### END Vars

Vagrant.require_version ">= 2.0.0"
Vagrant.configure("2") do |config|
  ### BEGIN common configuration
  # Install box with image from variable BOX
  config.vm.box = "#{BOX}"

  # Mount project folder explicitly
  config.vm.synced_folder './', '/vagrant'

  #VBOX specifics
  config.vm.provider "virtualbox" do |vb|
    vb.linked_clone = LINKED_CLONES
    if PACKET_CAPTURE
      vb.customize ["modifyvm", :id, "--nictrace2", "on", "--nictracefile2", "node-#{i+9}.pcap"]
    end
  end

  # Copy the self generated ssh private key to the machine
  # Caution: If you change filenames here, you have to:
  #   rename key files in this directory
  #   rename in start-scripts
  #   rename in init-master.sh script
  config.vm.provision "file", source: "./ssh/id_rsa_vagrant", destination: "/home/vagrant/.ssh/id_rsa_vagrant"

  # The path to the private key to use to SSH into the guest machine
  config.ssh.private_key_path = ["./ssh/id_rsa_vagrant", "./ssh/id_rsa_vagrant_insecure"]

  # No need to create new ssh keys, as we provided our self-generated keys with the shell provisioner above
  config.ssh.insert_key = false

  # Execute common tasks
  config.vm.provision "shell", path: "./scripts/init-common.sh", privileged: false
  ### END common configuration

  ### BEGIN configuration for nodes
  # Loop for every node from 1 to specified variable NODE_COUNT
  (1..NODE_COUNT).each do |i|
    # i-th node
    config.vm.define "node-#{i+9}" do |machine|
      # Set RAM and CPUs to the specified variables and create a master for linked clones to speed up the start
      machine.vm.provider "virtualbox" do |vb|
        vb.memory = NODE_RAM
        vb.cpus = NODE_CPUS

        if CEPH
            # Create our own controller for consistency and to remove VM dependency
            unless File.exist?("disk-#{i}-0.vdi")
              if BOX_HAS_SCSI
                # Renaming existing SCSI Controller
                vb.customize ['storagectl', :id,
                              '--name', 'SCSI',
                              '--rename', 'OSD Controller']
              else
                # Adding OSD Controller;
                # once the first disk is there assuming we don't need to do this
                vb.customize ['storagectl', :id,
                              '--name', 'OSD Controller',
                              '--add', 'scsi']
              end
            end

            (0..1).each do |d|
              vb.customize ['createhd',
                            '--filename', "disk-#{i}-#{d}",
                            '--size', '11000'] unless File.exist?("disk-#{i}-#{d}.vdi")
              vb.customize ['storageattach', :id,
                            '--storagectl', 'OSD Controller',
                            '--port', 3 + d,
                            '--device', 0,
                            '--type', 'hdd',
                            '--medium', "disk-#{i}-#{d}.vdi"]
            end
        end
      end

      machine.vm.provider :libvirt do |lv|
        lv.memory = NODE_RAM
        lv.cpus = NODE_CPUS
      end

      # Set hostname and IP according to the current run of the loop increased by 9
      # So the first node will have the IP 192.168.90.10, if SUBNET is set to 192.168.90
      machine.vm.hostname = "node-#{i+9}.local" #"node-#{'%03d' % i}"
      machine.vm.network "private_network", ip: "#{SUBNET}.#{i+9}"
    end
  end
  ### END configuration for nodes


  ### BEGIN configuration for master
  config.vm.define "master", primary: true do |machine|
    # Set RAM and CPUs to the specified variables
    machine.vm.provider "virtualbox" do |vb|
      vb.memory = MASTER_RAM
      vb.cpus = MASTER_CPUS
    end

    machine.vm.provider :libvirt do |lv|
      lv.memory = NODE_RAM
      lv.cpus = NODE_CPUS
    end

    # Set hostname of the machine
    machine.vm.hostname = "master.local"

    # Create a private network, which allows host-only access to the machine
    # using a specific IP.
    machine.vm.network "private_network", ip: "#{SUBNET}.2"

    # Run init-master.sh script inside the master vm
    # This will start the site.yml playbook
    # The playbook will initialize the kubernetes cluster with kubeadm and add nodes to the cluster
    machine.vm.provision "shell" do |s|
      s.path = "./scripts/init-master.sh"
      s.args = "#{INIT_ARGS}"
      s.env = {"ANSIBLE_CONFIG" => "/vagrant/ansible/ansible.cfg"}
      s.privileged = false
    end

    # Build a ceph Cluster if needed
    if CEPH
      machine.vm.provision "shell" do |s|
          s.path = "./scripts/init-ceph-ansible.sh"
          s.args = "#{INIT_ARGS}"
          s.env = {"ANSIBLE_CONFIG" => "/vagrant/ansible/ansible.cfg"}
          s.privileged = false
      end
    end
    machine.vm.provision "shell" do |s|
      s.path = "./scripts/init-msap-ansible.sh"
      s.args = "#{INIT_ARGS}"
      s.env = {"ANSIBLE_CONFIG" => "/vagrant/ansible/ansible.cfg"}
      s.privileged = false
    end
  end
  ### END Configuration for master
end
