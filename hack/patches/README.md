# Patches

If you need to patch anything not yet commited automatically in one of the submodules you have to create a folder named
after the submodule in here and put your patch file in there. The filename of the patch **must** end with `*.patch`
(e.g. `hack/patches/ceph-ansible/fix_xyz.patch`). The patch will then be automatically applied.