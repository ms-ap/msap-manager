#!/usr/bin/env python3
"""
This is a wrapper for a Vagrant environment with an integrated ansible master inside a VM.
Most Vagrant environments that work with ansible use the Vagrant ansible provisioner.
However this requires the user to have a suitable ansible installation on his host OS.
With the ansible master installed inside the VM this project can be started from any host OS
that supports Vagrant.
"""

# pylint: disable=import-error, invalid-name, unused-argument, unused-variable

__author__ = "Markus Greis"
__copyright__ = "Copyright 2018"
__version__ = "0.0.1"
__maintainer__ = "Markus Greis"
__email__ = "markus.greis@smail.inf.h-brs.de"
__status__ = "Development"

import os
import subprocess
import shlex
import logging
import re
import argparse
import configparser
import sys
import shutil
import yaml

# Vars
SETTINGS_FILE = 'settings.yml'
SETTINGS_SAMPLE = 'settings.yml.sample'
SCRIPT_PATH = os.path.abspath(__file__)
SCRIPT_DIR = os.path.dirname(SCRIPT_PATH)

# Logging
logger = logging.getLogger('msap')
logger.setLevel(logging.INFO)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
simple_formatter = logging.Formatter('%(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


def run_command(command, live_logging=True, shell_cmd=False, console_print=False, output_filter=''):
    """
    Run commands and live-log to console if in debugging mode

    :param command: Command which should be executed
    :param live_logging: Whether to log live to console or wait until command has finished
    :param shell_cmd: Whether it is an integrated shell command like "ls" or an external one
    :param console_print: Set to false to prevent double printing when in debug mode
    :param output_filter: Regular Expression to filter the console output
    :return: Returncode of the command
    """
    # Parse args in non-posix mode to allow windows path style
    cmd_args = shlex.split(command, posix=False)
    logger.info('Running command %s', cmd_args)

    # Use preferred python method for a subprocess, that doesn't need live-logging
    if not live_logging or logger.level < logging.DEBUG:
        res = subprocess.run(cmd_args)
        return res.returncode

    with subprocess.Popen(cmd_args, stdout=subprocess.PIPE, shell=shell_cmd) as proc:
        logger.debug('Command Output:')
        ch.setFormatter(simple_formatter)
        # Print lines while process is running
        while True:
            # Decode line from bytes to utf-8  String
            line = proc.stdout.readline().decode('utf-8')
            # If in debugging mode print every line
            if logger.level == logging.DEBUG:
                logger.debug('  %s', line.strip())
            # If not in debugging print only filtered output to console
            # This is useful for wrapping external commands and print only specific output
            elif console_print and output_filter is not None and re.search(output_filter, line):
                print(line.strip())

            # Wait for process to end and return some return code
            if line == '' and proc.poll() is not None:
                break
        ch.setFormatter(formatter)
        return proc.poll()


def generate_ssh_keys(keyfile):
    """
    Generate ssh keys that will be deployed inside the vagrant environment
    :param keyfile: The path to the private key of the keyfile which should be generated
    """
    # pylint: disable=too-many-locals
    logger.info('Generating ssh keypair.')
    from cryptography.hazmat.backends import default_backend
    from cryptography.hazmat.primitives.asymmetric import rsa
    from cryptography.hazmat.primitives import serialization

    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=4096,
        backend=default_backend()
    )

    private_key_bytes = private_key.private_bytes(serialization.Encoding.PEM,
                                                  serialization.PrivateFormat.PKCS8,
                                                  serialization.NoEncryption())
    public_key_bytes = private_key.public_key().public_bytes(serialization.Encoding.OpenSSH,
                                                             serialization.PublicFormat.OpenSSH)

    with open(keyfile, 'w') as priv, open(f'{keyfile}.pub', 'w') as pub:
        priv.write(bytes.decode(private_key_bytes))
        pub.write(bytes.decode(public_key_bytes))

    logger.info('Setting correct permissions for ssh keypair.')
    if os.name == 'nt':
        # Import modules
        import win32security
        import win32api
        import ntsecuritycon

        # Find the SID for the current user
        s_user, s_domain, s_type = win32security.LookupAccountName("", win32api.GetUserName())
        sd = win32security.GetFileSecurity(keyfile, win32security.DACL_SECURITY_INFORMATION)

        # Create a blank DACL and permit the user full access
        dacl = win32security.ACL()
        dacl.AddAccessAllowedAce(win32security.ACL_REVISION, ntsecuritycon.FILE_ALL_ACCESS, s_user)

        # Apply the DACL to the file
        sd.SetSecurityDescriptorDacl(1, dacl, 0)
        win32security.SetFileSecurity(keyfile, win32security.DACL_SECURITY_INFORMATION, sd)
    else:
        os.chmod(keyfile, 0o600)


def load_submodules():
    """
    Try to load all submodules
    """
    import git

    repo = git.Repo(os.getcwd())
    for sm in repo.submodules:
        if not sm.module_exists():
            logger.info('Initializing git submodule %s', sm.name)
            # Calling git directly for own submodules
            # since using relative path is not working in gitpython
            # see https://github.com/gitpython-developers/GitPython/issues/730
            if sm.url[:3] == '../':
                repo.git.submodule('init', sm.name)
                repo.git.submodule('update', sm.name)
                assert sm.module_exists(), f'The submodule {sm.name} is not existing and ' \
                                           f'cannot be updated. Check if you are able ' \
                                           f'to clone {sm.url}'
            # For external submodules we can use the update function of gitpython
            else:
                sm.update()

        # Apply patches if there are any
        patches = os.path.join('hack', 'patches', sm.path)
        if not os.path.exists(patches):
            continue
        # pylint: disable=line-too-long
        for patch in [f for f in os.scandir(os.path.abspath(patches)) if f.name.endswith('.patch')]:
            try:
                sm_repo = git.Repo(sm.path)
                sm_repo.git.apply(patch.path)
                logger.info('Applied patch "%s" to "%s"', patch.name, sm.path)
                # Ignore these exceptions as they shouldn't be critical
            # pylint: disable=no-member
            except git.exc.GitCommandError:
                pass


def prerun_checks():
    """
    Check if requirements are met
    """
    # Check if submodules are loaded
    load_submodules()

    # Check if Vagrant is in Path
    try:
        res = subprocess.run(shlex.split('vagrant --version'), stdout=subprocess.PIPE)
        logger.debug('Found %s', bytes(res.stdout).decode('utf-8'))
    except FileNotFoundError:
        logger.error('Could not find Vagrant. Either it\'s not installed or not in Path.\n'
                     '\tYou can get it here: https://www.vagrantup.com/downloads.html')
        sys.exit(1)

    # Create ssh keypair if not existing
    ssh_keyfile = os.path.join('ssh', 'id_rsa_vagrant')
    if not os.path.isfile(ssh_keyfile):
        generate_ssh_keys(ssh_keyfile)


def create_config(filename):
    """
    Creates a default configuration

    :rtype: dict
    :param: filename: Filepath to config file
    :return: dict with settings
    """

    # Copy settings.yml.sample to settings.yml
    shutil.copyfile(SETTINGS_SAMPLE, filename)

    with open(filename, 'r') as f:
        try:
            config = yaml.safe_load(f)
            return config
        except yaml.YAMLError as e:
            logger.error('There seem to be an error in your %s file. '
                         'The recreation of the settings file '
                         'failed with error:\n\t%s\n', SETTINGS_SAMPLE, e)
            exit(1)


def load_config(filename):
    """
    Loads Configuration

    :rtype: dict
    :param filename: Filepath to config file
    :return: dict with settings
    """

    # First check if settings.yml file exists.
    # This can't be done with except FileNotFoundError because configParser ignores this exception
    if not os.path.isfile(filename):
        logger.warning('%s could not be found. Creating new one.', SETTINGS_FILE)
        return create_config(filename)

    with open(filename, 'r') as f:
        try:
            config = yaml.safe_load(f)
        except yaml.YAMLError as e:
            logger.warning('There were the following errors while parsing %s:\n'
                           '\t%s\n'
                           'Creating new one.', SETTINGS_FILE, e)
            shutil.copyfile(filename, f'{filename}.bak')
            config = create_config(filename)

    return config


def update_vagrant_vars(settings, vagrant_vars_file='vagrant_variables.yml'):
    """
    Update the variables for the Vagrantfile automatically based on user input

    :param settings: yaml representation of settings.yml
    :param vagrant_vars_file: Path to Vagrant variables file in yaml format
    """
    # Load Vagrantfile case-insensitive
    if not os.path.isfile(vagrant_vars_file):
        vagrant_vars_file = vagrant_vars_file.casefold()

    # Set variables
    with open(vagrant_vars_file, 'w') as f:
        f.write(f'# THIS FILE IS GENERATED BY {os.path.basename(__file__)} '
                'DO NOT EDIT BY HAND!\n\n')
        yaml.safe_dump(settings['vagrant'], f)


def update_ansible_hosts(settings, hostsfile=os.path.join('ansible', 'hosts')):
    """
    Create the ansbile hosts file automatically based on user inputs

    :param settings: ConfigParser which hold settings
    :param hostsfile: Path to ansibles inventory file
    """
    # Set Variables
    subnet = settings['vagrant']['subnet']
    master_ip = 2
    first_node_ip = 10
    last_node_ip = int(settings['vagrant']['node_count']) + 9

    # Allow keys without value, since that's the default case in ansbile hosts file and
    # remove ':' as delimiter to allow range notations like [10:15]
    config = configparser.ConfigParser(allow_no_value=True, delimiters='=')
    master = f'{subnet}.{master_ip}'
    nodes = f'{subnet}.[{first_node_ip}:{last_node_ip}]'
    if settings['vagrant']['node_count'] == 0:
        nodes = f'# {nodes}'

    # Create the hosts file
    config['master'] = {
        master: None
    }

    config['nodes'] = {
        '# Caution: If you change the number here, '
        'you must also change the "node_count" variable in the Vagrantfile': None,
        nodes: None
    }

    # If python interpreter is set, apply it to inventory
    if settings['ansible']['python_interpreter'] is not None:
        config['hosts:children'] = {
            'master': None,
            'nodes': None
        }

        config['hosts:vars'] = {
            'ansible_python_interpreter': settings['ansible']['python_interpreter']
        }

    # If ceph is true add ceph specific hosts to inventory
    if settings['vagrant']['ceph']:
        for role in [f for f in settings['ceph_roles'] if settings['ceph_roles'][f] is not None]:
            host = settings['ceph_roles'][role]
            config[f'{role}:children'] = {
                host: None
            }

    # Write the hosts file
    with open(hostsfile, 'w') as f:
        f.write(f'# THIS FILE IS GENERATED BY {os.path.basename(__file__)} '
                'DO NOT EDIT BY HAND!\n\n')
        config.write(f, space_around_delimiters=False)


def update_ansible_group_vars(settings, filename=os.path.join('ansible', 'group_vars', 'all')):
    """
    :param settings: YAML which hold settings
    :param filename: Path to the group_vars file
    """
    # Create directory if it not exists
    if not os.path.exists(filename):
        os.makedirs(os.path.dirname(filename))

    with open(filename, 'w') as f:
        f.write(f'# THIS FILE IS GENERATED BY {os.path.basename(__file__)} '
                'DO NOT EDIT BY HAND!\n\n')
        yaml.safe_dump(settings['ansible']['group_vars']['all'], f)


def addbox(boxpath='boxes', boxvendor='netlab'):
    """
    Searches for a boxfile and adds it to vagrant
    The boxfile should be named in the form: <BOXNAME>-<BOXVERSION>.box

    :param boxvendor: Vendor of the box like ubuntu/xenial64 where ubuntu is the vendor
    :param boxpath: Path to the boxfiles
    """
    # Get installed boxes
    installed_boxes = subprocess.run(shlex.split('vagrant box list'), stdout=subprocess.PIPE)

    # Create dictionary with boxname: boxversion
    boxlist = {}
    for box in bytes.decode(installed_boxes.stdout, 'utf-8').split('\n'):
        if box == "":
            boxname, boxversion = box.split()[0], box.split()[2].strip('()')
            boxlist[boxname] = boxversion

    # Search for .box files in boxes directory
    try:
        with os.scandir(boxpath) as it:
            for entry in it:
                # Check if it's a boxfile
                if entry.name.endswith('.box') and entry.is_file():
                    # Get filename without extension
                    filename = os.path.splitext(entry.name)[0]

                    # Split file <BOXNAME>-<BOXVERSION>.box into boxname and boxversion
                    boxname, boxversion = filename.split('-')
                    boxname = f'{boxvendor}/{boxname}'

                    # Call Vagrant command to add the boxfile
                    if boxname not in boxlist:
                        logger.debug('Trying to add %s to vagrant.', os.path.abspath(entry))
                        run_command(f'vagrant box add --name {boxname} {os.path.relpath(entry)}')
    except FileNotFoundError:
        logger.debug('%s folder could not be found - Skipping vagrant add box.', boxpath)
    except ValueError:
        logger.error('Boxfile has an invalid name. The filename has to have the following form:\n'
                     '\t <BOXNAME>-<BOXVERSION>.box')

    sys.exit(1)


def start(args):
    """
    Start cluster

    :param args: commandline arguments
    """
    # Load default values from settings.yml
    settings = load_config(SETTINGS_FILE)

    # Update default settings with command line arguments
    settings['vagrant'].update(
        {
            'box': shlex.split(args.vagrant_box)[0],
            'node_count': args.vagrant_node_count,
            'packet_capture': args.vagrant_packet_capture,
            'provider': shlex.split(args.vagrant_provider)[0],
            'provision': args.vagrant_provision,
            'ceph': args.vagrant_ceph
        })

    # Update default settings with command line arguments
    settings['ansible']['group_vars']['all']['kubernetes'].update(
        {
            'cni': shlex.split(args.cni)[0],
            'cri': shlex.split(args.cri)[0]
        })

    # Update default settings with command line arguments
    settings['ansible']['group_vars']['all'].update(
        {
            'management_tool': shlex.split(args.management_tool)[0]
        })

    # Update vagrant and ansible configurations to match the new settings
    update_vagrant_vars(settings)
    update_ansible_hosts(settings)
    update_ansible_group_vars(settings)

    # Start vagrant
    logger.info('Starting Vagrant. This will take a few minutes.')
    if settings['vagrant']['provision']:
        run_command(f'vagrant up --provision --provider {settings["vagrant"]["provider"]}')
    else:
        run_command(f'vagrant up --provider {settings["vagrant"]["provider"]}')


def stop(args):
    """
    Stop cluster

    :param args: commandline arguments
    """
    run_command(f'vagrant halt {" ".join(args.nodes)}')


def resume(args):
    """
    Resume stopped machines

    :param args: commandline arguments
    """
    run_command(f'vagrant up --no-provision {" ".join(args.nodes)}')


def restart(args):
    """
    Restart virtual machines

    :param args: commandline arguments
    """
    run_command('vagrant reload --no-provision')


def reset(args):
    """
    Reset kubernetes cluster without changing vms running state

    :param args: commandline arguments
    """
    args.ssh_dest = 'master'
    settings = load_config(SETTINGS_FILE)
    ssh(args, f'ansible-playbook {settings["ansible"]["playbook_path"]}/reset_site.yml')


def rebuild(args):
    """
    Rebuild kubernetes cluster without changing vms running state

    :param args: commandline arguments
    """
    args.ssh_dest = 'master'
    settings = load_config(SETTINGS_FILE)
    if settings['vagrant']['init_args']:
        ssh(args, f'ansible-playbook {settings["vagrant"]["init_args"]} '
            f'--extra-vars cni={args.cni} {settings["ansible"]["playbook_path"]}/site.yml')
    else:
        ssh(args, f'ansible-playbook '
            f'--extra-vars cni={args.cni} {settings["ansible"]["playbook_path"]}/site.yml')


def clean(args):
    """
    Stop and delete machines

    :param args: commandline arguments
    """
    run_command('vagrant destroy -f')


def list_vms(args):
    """
    List virtual machines

    :param args: commandline arguments
    """
    run_command('vagrant status', console_print=True, output_filter='(master|node)')


def ssh(args, ssh_command=None):
    """
    SSH into a virtual machine

    :param args: commandline arguments
    :param ssh_command: commandline arguments that should be executed inside VM
    """
    if ssh_command is not None:
        res = run_command(f'vagrant ssh -c "{ssh_command}" {args.ssh_dest}', live_logging=False)
    else:
        res = run_command(f'vagrant ssh {args.ssh_dest}', live_logging=False)

    if res == 1:
        logger.error('Unable to ssh into "%s".\n'
                     '\t First make sure the vm is powered on by running "%s ls".\n'
                     '\t If you still get this error try to delete all files '
                     'except of id_rsa_vagrant_insecure from the ssh directory, '
                     'then clean the cluster and start again.', args.ssh_dest, __file__)


def generate_inventory(args):
    """
    Generates a new inventory from default settings

    :param args: commandline arguments
    :param filename: filename of settings.yml file to parse:
    """
    config = load_config(SETTINGS_SAMPLE)

    vars_production = os.path.join(
        'msap-ansible', 'inventories', 'production', 'group_vars', 'all.yml.sample')
    vars_ceph = os.path.join(
        'msap-ansible', 'inventories', 'production-ceph', 'group_vars', 'all.yml.sample')

    production_string = f'### BEGIN MSAP_ANSIBLE ###\n' \
                        f'{yaml.safe_dump(config[".msap_ansible"])}' \
                        f'{yaml.safe_dump(config["shared"])}' \
                        f'### END MSAP_ANSIBLE ###\n\n'

    ceph_string = f'### BEGIN CEPH_ANSIBLE ###\n' \
                  f'{yaml.safe_dump(config[".ceph_ansible"])}' \
                  f'### END CEPH_ANSIBLE ###\n\n'

    if args.generate:
        with open(vars_production, 'w') as production, open(vars_ceph, 'w') as ceph:
            production.write(production_string)
            ceph.write(production_string)
            ceph.write(ceph_string)

    if args.print:
        print(production_string)
        print(ceph_string)



def main():
    """
    main
    """
    os.chdir(SCRIPT_DIR)
    # Run prerun checks first
    prerun_checks()

    # Load settings.yml
    settings = load_config(SETTINGS_FILE)

    # Add Parsers
    parser = argparse.ArgumentParser(description='Creates a virtual environment '
                                                 'for kubeadm or docker swarm with vagrant')
    parser.add_argument('-q', '--quiet',
                        dest='loglevel',
                        action='store_const',
                        const=logging.INFO,
                        default=logging.DEBUG,
                        help='log only warnings and errors')

    subparsers = parser.add_subparsers(help='sub-command help')

    # create the parser for the start command
    parser_start = subparsers.add_parser('start', help='start the virtual cluster')
    parser_start.add_argument('-m', '--management-tool',
                              dest='management_tool',
                              default=settings['ansible']['group_vars']['all']['management_tool'],
                              action='store',
                              choices=['kubernetes', 'swarm'],
                              help='Choose the managament tool which should be used')
    parser_start.add_argument('-n', '--cni', '--container-network-interface',
                              dest='cni',
                              default=settings['ansible']['group_vars']['all']['kubernetes']['cni'],
                              action='store',
                              choices=['calico', 'canal', 'flannel',
                                       'kube-router', 'romana', 'weavenet'],
                              help='Choose the CNI (container network interface) which should '
                                   'be used [this setting only applies to kubernetes clusters]')
    parser_start.add_argument('-r', '--cri', '--container-runtime-interface',
                              dest='cri',
                              default=settings['ansible']['group_vars']['all']['kubernetes']['cri'],
                              action='store', choices=['docker', 'containerd', 'crio'],
                              help='Choose the CRI (container runtime interface) which should '
                                   'be used [this setting only applies to kubernetes clusters]')
    parser_start.add_argument('-c', '--node-count',
                              metavar='INT',
                              type=int,
                              dest='vagrant_node_count',
                              default=settings['vagrant']['node_count'],
                              action='store',
                              help='Set the number of nodes which should be started')
    parser_start.add_argument('-b', '--box',
                              metavar='VAGRANT_BOX',
                              dest='vagrant_box',
                              default=settings['vagrant']['box'],
                              action='store',
                              help='Set the vagrant box which should be used')
    parser_start.add_argument('-p', '--provision',
                              dest='vagrant_provision',
                              default=settings['vagrant']['provision'],
                              action='store_true',
                              help='Tell vagrant to reprovision')
    parser_start.add_argument('--ceph',
                              dest='vagrant_ceph',
                              default=settings['vagrant']['ceph'],
                              action='store_true',
                              help='If this is specified a ceph cluster '
                                   'will be deployed with the nodes as OSDs and MONs')
    parser_start.add_argument('--packet-capture', dest='vagrant_packet_capture',
                              const='true',
                              default=settings['vagrant']['packet_capture'],
                              action='store_const',
                              help='Enable packet capture for VMs')
    parser_start.add_argument('--provider',
                              dest='vagrant_provider',
                              default=settings['vagrant']['provider'],
                              action='store', choices=['virtualbox', 'vmware', 'libvirt'],
                              help='Choose the Vagrant provider which should be used')
    parser_start.set_defaults(func=start)

    # create the parser for the stop command
    parser_stop = subparsers.add_parser('stop', help='stop the virtual cluster')
    parser_stop.add_argument('nodes',
                             metavar='NODE',
                             nargs='*',
                             help='space separated list of nodes that should be stopped')
    parser_stop.set_defaults(func=stop)

    # create the parser for the resume command
    parser_resume = subparsers.add_parser('resume',
                                          help='start a previously stopped virtual cluster')
    parser_resume.add_argument('nodes',
                               metavar='NODE',
                               nargs='*',
                               help='space separated list of nodes that should be resumed')
    parser_resume.set_defaults(func=resume)

    # create the parser for the restart command
    parser_restart = subparsers.add_parser('restart',
                                           help='restart virtual machines '
                                                'and load new Vagrantfile configuration')
    parser_restart.set_defaults(func=restart)

    # create the parser for the reset command
    parser_reset = subparsers.add_parser('reset',
                                         help='reset the kubernetes cluster inside '
                                              'the virtual machines without changing '
                                              'the virtual machines running state')
    parser_reset.set_defaults(func=reset)

    # create the parser for the rebuild command
    parser_rebuild = subparsers.add_parser('rebuild',
                                           help='rebuild the kubernetes cluster inside '
                                                'the virtual machines (should be run after reset)')
    parser_rebuild.add_argument('-n', '--container-network-interface',
                                dest='cni',
                                default=settings['ansible']['group_vars']['all']
                                ['kubernetes']['cni'], action='store',
                                choices=['calico', 'canal',
                                         'flannel', 'kube-router', 'romana', 'weavenet'],
                                help='Choose the CNI (container network interface) which should '
                                     'be used [this setting only applies to kubernetes clusters]')
    parser_rebuild.set_defaults(func=rebuild)

    # create the parser for the clean command
    parser_clean = subparsers.add_parser('clean', help='kill all virtual machines in the cluster'
                                                       ' and delete them')
    parser_clean.set_defaults(func=clean)

    # create the parser for the list command
    parser_list = subparsers.add_parser('ls', help='list the virtual machines and their state')
    parser_list.set_defaults(func=list_vms)

    # create the parser for the ssh command
    parser_ssh = subparsers.add_parser('ssh', help='connect to a virtual machine via ssh')
    parser_ssh.add_argument('ssh_dest',
                            metavar='NAME|ID',
                            action='store',
                            help='Provide the machine name you would like to ssh into. '
                            f'You can get this via {__file__} ls')
    parser_ssh.set_defaults(func=ssh)

    # create the parser for the config command
    parser_config = subparsers.add_parser('config', help='manage config files')
    parser_config.add_argument('--print',
                               action='store_true',
                               help='print new group_vars files')
    parser_config.add_argument('--generate',
                               action='store_true',
                               help='generate new group_vars files (in msap-ansible/inventories)')
    parser_config.set_defaults(func=generate_inventory)

    # Parse command line arguments
    args = parser.parse_args()

    # Set loglevel based on user input
    logger.setLevel(args.loglevel)
    logger.debug(args)

    # Call functions based on user input
    try:
        args.func(args)
    except AttributeError as e:
        parser.print_help()
        logger.debug(e)


if __name__ == '__main__':
    main()
