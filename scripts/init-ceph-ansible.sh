#!/bin/sh

# Variables
delay=10
retries=5
os_id=$(cat /etc/os-release | awk -F "=" '/^ID_LIKE=/ {gsub(/"/,""); print $2}')
if [ -z "$os_id" ]; then os_id=$(cat /etc/os-release | awk -F "=" '/^ID=/ {gsub(/"/,""); print $2}'); fi
python_interpreter=$(awk -F '/' '/ansible_python_interpreter/ { print $NF }' /vagrant/ansible/hosts | tr -d '\r')
# Use "python" as default interpreter if none is set
python_interpreter=${python_interpreter:=python}
# Set pip version
if [ $python_interpreter = "python" ];then  python_pip=pip; else python_pip=pip3; fi
pip_exec=$(which $python_pip)

# Parse command line arguments
# The following arguments will be applied to the ansible-playbook command
while [ $# -gt 0 ]
do
  if [ -z "$ansible_args" ]; then
    ansible_args="$1"
    shift
    continue
  fi
  ansible_args="$ansible_args $1"
  shift
done

# Main
# Install requirements for ceph-ansible
sudo -H $pip_exec install -r /vagrant/ceph-ansible/requirements.txt
if [ $? -ne 0 ]; then exit 1; fi
# Execute ceph-ansible
# First copy the sample file over to a yml file
cp /vagrant/ceph-ansible/site.yml.sample /vagrant/ceph-ansible/site.yml
cp /vagrant/ceph-ansible/site-docker.yml.sample /vagrant/ceph-ansible/site-docker.yml
# Then copy the plugins to the default ansible directories, since we want to use our own ansible.cfg file
sudo mkdir -p /usr/share/ansible/plugins
sudo cp -r /vagrant/ceph-ansible/plugins/actions /usr/share/ansible/plugins/action
sudo cp -r /vagrant/ceph-ansible/plugins/callback /usr/share/ansible/plugins/callback
sudo cp -r /vagrant/ceph-ansible/library /usr/share/ansible/plugins/modules
# Then run the playbook
ansible-playbook $ansible_args /vagrant/ceph-ansible/site.yml
if [ $? -ne 0 ]; then exit 1; fi

# For time measures
#/usr/bin/time -v ansible-playbook $ansible_args /vagrant/ceph-ansible/site.yml

# This ensures that all files in vagrant home dir belongs to vagrant user
sudo chown -R vagrant:vagrant /home/vagrant
