#!/bin/sh

# Variables
delay=10
retries=5
os_id=$(cat /etc/os-release | awk -F "=" '/^ID_LIKE=/ {gsub(/"/,""); print $2}')
if [ -z "$os_id" ]; then os_id=$(cat /etc/os-release | awk -F "=" '/^ID=/ {gsub(/"/,""); print $2}'); fi
python_interpreter=$(awk -F '/' '/ansible_python_interpreter/ { print $NF }' /vagrant/ansible/hosts | tr -d '\r')
# Use "python" as default interpreter if none is set
python_interpreter=${python_interpreter:=python}

# Prepare authorized_keys
rm /home/vagrant/.ssh/authorized_keys
awk '{print $0}' /vagrant/ssh/*.pub >> /home/vagrant/.ssh/authorized_keys

# Change permissions for private key
# Caution: If you change filenames here, you have to: rename in Vagrantfile, rename in start-scripts
chmod 700 /home/vagrant/.ssh/id_rsa_vagrant
# Change permissions for authorized_keys file
chmod 600 /home/vagrant/.ssh/authorized_keys

# This ensures that all files in vagrant home dir belongs to vagrant user
chown -R vagrant:vagrant /home/vagrant

echo "Check if $python_interpreter is already installed"
$python_interpreter --version
if [ $? -gt 0 ]; then
  echo "$python_interpreter not found. Trying to install it"
  echo "Detected $os_id operating system"
  # Retry install a few times
  rc=1
  while [ $rc -ne 0 ] && [ $retries -ge 0 ]; do
    rc=0
    # Install python for rhel linux systems
    if [ $(echo "$os_id" | grep -cE ".*fedora.*") -ge 1 ]; then
      sudo yum install epel-release
      if [ $? -ne 0 ]; then rc=1; fi
      # Check if we want to install python or python3
      if [ $python_interpreter = "python" ]; then
        sudo yum install -y python
      else
        sudo yum install -y ansible python36
        # sudo ln -s /usr/bin/python36 /usr/bin/python3
      fi
      if [ $? -ne 0 ]; then rc=1; fi

    # Install python for debian linux systems
    elif [ $(echo "$os_id" | grep -cE ".*debian.*") -ge 1 ]; then
      sudo apt-get update
      if [ $? -ne 0 ]; then rc=1; fi
      # Check if we want to install python or python3
      if [ $python_interpreter = "python" ]; then
        sudo apt-get install -y python
      else
        sudo apt-get install -y python3
      fi
      if [ $? -ne 0 ]; then rc=1; fi
    else
      echo "Unsupported OS-Version: $os_id. Exiting with error."
      exit 1
    fi

    # Increase counter if there was an error
    if [ $rc -ne 0 ]; then
      echo "Retrying $retries times in $delay seconds"
      retries=$(expr $retries - 1)
      sleep "$delay"
    fi
  done
  if [ $rc -ne 0 ]; then
    echo "Could not install all necessary packages. Exiting with error code 1"
    exit 1
  fi
fi