#!/bin/sh

# Variables
delay=10
retries=5
os_id=$(cat /etc/os-release | awk -F "=" '/^ID_LIKE=/ {gsub(/"/,""); print $2}')
if [ -z "$os_id" ]; then os_id=$(cat /etc/os-release | awk -F "=" '/^ID=/ {gsub(/"/,""); print $2}'); fi
python_interpreter=$(awk -F '/' '/ansible_python_interpreter/ { print $NF }' /vagrant/ansible/hosts | tr -d '\r')
# Use "python" as default interpreter if none is set
python_interpreter=${python_interpreter:=python}
# Set pip version
if [ $python_interpreter = "python" ];then  python_pip=pip; else python_pip=pip3; fi

# Parse command line arguments
# The following arguments will be applied to the ansible-playbook command
while [ $# -gt 0 ]
do
  if [ -z "$ansible_args" ]; then
    ansible_args="$1"
    shift
    continue
  fi
  ansible_args="$ansible_args $1"
  shift
done

# Main
echo "Check if $python_pip is already installed"
$python_pip --version
# Install pip if needed
if [ $? -gt 0 ]; then
  echo "$python_pip not found. Trying to install it"
  echo "Detected $os_id operating system"
  # Retry install a few times
  rc=1
  while [ $rc -ne 0 ] && [ $retries -ge 0 ]; do
    rc=0
    # Install pip for rhel linux systems
    if [ $(echo "$os_id" | grep -cE ".*fedora.*") -ge 1 ]; then
      sudo yum install epel-release
      if [ $? -ne 0 ]; then rc=1; fi
      # Check if we want to install pip or pip3
      if [ $python_pip = "pip" ]; then
        sudo yum install -y python-pip
      else
        sudo yum install -y python36-pip
        sudo /usr/bin/pip3 install --upgrade pip
      fi
      if [ $? -ne 0 ]; then rc=1; fi

    # Install pip for debian linux systems
    elif [ $(echo "$os_id" | grep -cE ".*debian.*") -ge 1 ]; then
      sudo apt-get update
      if [ $? -ne 0 ]; then rc=1; fi
      # Check if we want to install pip or pip3
      if [ $python_pip = "pip" ]; then
        sudo apt-get install -y python-pip
      else
        sudo apt-get install -y python3-pip
      fi
      if [ $? -ne 0 ]; then rc=1; fi
    else
      echo "Unsupported OS-Version: $os_id. Exiting with error."
      exit 1
    fi

    # Increase counter if there was an error
    if [ $rc -ne 0 ]; then
      echo "Retrying $retries times in $delay seconds"
      retries=$(expr $retries - 1)
      sleep "$delay"
    fi
  done
  if [ $rc -ne 0 ]; then
    echo "Could not install all necessary packages. Exiting with error code 1"
    exit 1
  fi
fi


# Create new environment variable
# This will tell ansible where to find its configuration for vagrant
# and will make ansible-playbook commands work from everywhere
grep '^ANSIBLE_CONFIG' /etc/environment > /dev/null 2>&1
if [ $? -gt 0 ]; then
  echo 'ANSIBLE_CONFIG=/vagrant/ansible/ansible.cfg' | sudo tee --append /etc/environment
fi

# This ensures that all files in vagrant home dir belongs to vagrant user
sudo chown -R vagrant:vagrant /home/vagrant
