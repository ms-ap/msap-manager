---
### BEGIN SHARED SETTINGS ###
# settings for both ansible and vagrant
shared: &SHARED
  # the subnet which will be used for connection (currently only netmask /24 is supported)
  subnet: 192.168.90
### END SHARED SETTINGS ###

### BEGIN VAGRANT SETTINGS ###
# settings that apply to vagrant and it's provisioners
vagrant:
  <<: *SHARED # MERGE SHARED SETTINGS - DO NOT EDIT THIS LINE
  # this configures what box the machine will be brought up against
  # see https://app.vagrantup.com/boxes/search for available boxes
  box: ubuntu/bionic64
  master_ram: 2048
  master_cpus: 2
  # this configures how many nodes should be started
  node_count: 3
  node_ram: 2048
  node_cpus: 2
  # this configures the command line arguments which are passed to the init script (to ansible-playbook command)
  init_args:
  # comma separated list of prepared vagrant boxes
  prepared_boxes: netlab/kubeadm
  # this configures additional command line arguments only for prepared boxes
  prepared_boxes_extra_args: --skip-tags=apt,install
  # setting this to true will speed up cluster start, but won't delete the master vm automatically
  linked_clones: false
  # setting this to true will create a packet capture of the subnet's network interface
  packet_capture: false
  # this configures the provider for vagrant
  provider: virtualbox
  # setting this to true will tell vagrant to always reprovision on start
  provision: false
  # if set to true, a ceph cluster will be deployed with the nodes as OSDs and MONs
  ceph: false
  # set this to true if your box already has a default scsi controller
  box_has_scsi: true
  # the disks Vagrant should create
  disks: &DISKS
    - data: /dev/sdc
    - data: /dev/sdd
### END VAGRANT SETTINGS ###

### BEGIN MSAP_ANSIBLE SETTINGS ###
.msap_ansible: &MSAP_ANSIBLE
  # common settings
  timezone: Europe/Berlin
  # has to be swarm or kubernetes
  management_tool: kubernetes
  # should be left empty if you are using ubuntu. For Debian use the corresponding ubuntu version
  # see https://askubuntu.com/questions/445487/what-debian-version-are-the-different-ubuntu-versions-based-on
  ppa_codename:
  # kubeadm specific settings
  kubernetes:
    # see https://github.com/kubernetes/kubernetes/releases for available versions
    version: 1.14.1
    # has to be one of docker, containerd, crio
    cri: docker
    # set to true to enable kubernetes dashboard
    enable_dashboard: true
    # set to true to enable kubernetes proxy
    enable_proxy: false
    # must be between 30000 and 32767
    dashboard_port: 30080
    kube_proxy_port: 8001
    # has to be one of: calico, canal, flannel, kube-router, romana, weavenet
    cni: weavenet
    # if true, kubelet will be started with --node-ip=subnet.IP command
    custom_node_ip: true
    # specify which users should be able to execute kubectl commands
    config:
      - home: /root
        user: root
        group: root
      - home: /home/vagrant/
        user: vagrant
        group: vagrant
  # settings for the container runtime interfaces
  cri_settings:
    # set which cgroup driver should be used. MUST be either cgroupfs OR systemd
    cgroup_driver: systemd
    # settings for docker
    docker:
      # url and dest will only be used if installation via package manager fails
      url: https://get.docker.com/
      dest: /tmp/docker-installer.sh
      # see https://docs.docker.com/engine/release-notes/ for available versions
      version: 5:18.09.2
    # settings for containerd
    containerd:
      # see https://storage.googleapis.com/cri-containerd-release/ for available versions
      version: 1.2.5
    # settings for cri-o
    crio:
      # see https://launchpad.net/~projectatomic/+archive/ubuntu/ppa for available apt versions
      # this HAS TO BE the same as the kubernetes version
      version: 1.14
      # see https://cbs.centos.org/repos/ in the folder paas7-crio-* for available centos versions
      # the version variable above will be ignored in centos since only one versions exists in the repo
      yum_repo: https://cbs.centos.org/repos/paas7-crio-114-release/x86_64/os/
  # settings for docker swarm
  swarm:
    # set the swarm advertise port
    master_advertise_port: 2377
  # Specify a list of additional packages that will be installed at the beginning of the play
  additional_apt_packages:
    - nfs-common
  additional_yum_packages:
    - nfs-utils
  # settings for monitoring
  monitoring:
    # set to true to enable monitoring
    enabled: true
    # must be between 30000 and 32767
    grafana_port: 30300
    prometheus_port: 30090
    # see https://github.com/coreos/kube-prometheus for available versions
    kube_prometheus_version: release-0.1
### END MSAP_ANSIBLE SETTINGS ###

### BEGIN CEPH ROLES ###
ceph_roles:
  # Format is <CephRole: VagrantHost>
  mons: nodes
  osds: nodes
  mdss: master
  rgws: master
  nfss: master
  mgrs: master
  rbdmirrors:
  clients:
  iscsigws:
### END CEPH ROLES ###

### BEGIN CEPH_ANSIBLE SETTINGS ###
# see https://github.com/ceph/ceph-ansible for documentation
.ceph_ansible: &CEPH_ANSIBLE
  ceph_origin: repository
  ceph_repository: community
  # remember to change both ceph_dev_branch and nfs_ganesha_flavor if you want the master branch
  ceph_stable_release: nautilus
  # these have to match with the "subnet" variable
  public_network: 192.168.90.0/24
  cluster_network: 192.168.90.0/24
  monitor_address_block: 192.168.90.0/24
  # this has to be the same as the master ip
  radosgw_address: 192.168.90.2
  # using lvm_volumes instead of devices since devices is not used in the 'third' scenario
  # see https://github.com/ceph/ceph-ansible/blob/stable-3.2/group_vars/osds.yml.sample#L37
  lvm_volumes: *DISKS
  osd_scenario: lvm
  # use this to avoid permission issues
  ceph_nfs_rgw_squash: No_Root_Squash
  ceph_nfs_ceph_squash: No_Root_Squash
  # enable FSAL CEPH
  nfs_file_gw: true
  # enable NFS Object access (FSAL RGW) also enable rgws in ceph_roles setting when using this
  nfs_obj_gw: true
  # use version 2.7 of nfs-ganesha since this is currently the only one which supports bionic
  nfs_ganesha_stable_branch: V2.7-stable
  # copy ceph admin key to all nodes to allow mounting of the nfs volume
  copy_admin_key: true
  # FIXME: Delete this when https://github.com/ceph/ceph-ansible/issues/3789 or https://tracker.ceph.com/issues/39268 is closed
  nfs_ganesha_stable_deb_repo: "[trusted=yes] https://chacra.ceph.com/r/nfs-ganesha-stable/V2.7-stable/2356c3867730696aacc31874357b3499062fc902/ubuntu/bionic/flavors/ceph_nautilus"
### END CEPH_ANSIBLE SETTINGS ###

### BEGIN ANSIBLE SETTINGS ###
# settings that apply to ansible and the ansible playbooks
ansible:
  # Path to the ansible playbooks
  playbook_path: /vagrant/msap-ansible
  # Set the python interpreter of the target systems
  python_interpreter: /usr/bin/python3
  group_vars:
    all:
      <<: *SHARED # MERGE SHARED SETTINGS - DO NOT EDIT THIS LINE
      <<: *MSAP_ANSIBLE # MERGE MSAP SETTINGS - DO NOT EDIT THIS LINE
      <<: *CEPH_ANSIBLE # MERGE CEPH SETTINGS - DO NOT EDIT THIS LINE
### END ANSIBLE SETTINGS ###